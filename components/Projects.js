import Image from "next/image";
import Link from "next/link";
import React from "react";
import ProjectItem from "./ProjectItems";
import Car from "../public/assets/projects/Cardealership.png";
import CrusieControl from "../public/assets/projects/CruiseControl.png";
import ConferenceGo from "../public/assets/projects/ConferenceGo.png";

const Projects = () => {
  return (
    <div id="projects" className="w-full">
      <div className="max-w-[1240px] mx-auto px-2 py-16">
        <p className="text-xl tracking-widest uppercase text-[#317773]">
          Projects
        </p>
        <h2 className="py-4">What I&apos;ve Built</h2>
        <div className="grid md:grid-cols-2 gap-8">
          <ProjectItem
            title="CruiseControl"
            backgroundImg={CrusieControl}
            projectUrl="/CruiseControl"
            tech="React JS"
          />
          <ProjectItem
            title="Conference Go"
            backgroundImg={ConferenceGo}
            projectUrl="/ConferenceGo"
            tech="React JS"
          />
          <ProjectItem
            title="Car Dealership"
            backgroundImg={Car}
            projectUrl="/CarDealership"
            tech="React JS"
          />
        </div>
      </div>
    </div>
  );
};

export default Projects;
