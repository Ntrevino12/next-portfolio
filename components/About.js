import React from "react";
import Image from "next/image";
import Link from "next/link";
import AboutImg from "../public/assets/about.jpg";

const About = () => {
  return (
    <div id="about" className="w-full md:h-screen p-2 flex items-center py-16">
      <div className="max-w-[1240px] m-auto md:grid grid-cols-3 gap-8">
        <div className="col-span-2">
          <p className="uppercase text-xl tracking-widest text-[#317773]">
            About
          </p>
          <h2 className="py-4">Who I Am</h2>
          <p className="py-2 text-gray-600">
            I have always been drawn to personal growth and the exciting
            possibilities within the tech industry. This led me to embark on a
            journey to become a Full Stack Software Engineer, starting with
            pursuing a degree at a local college near me. However, I encountered
            a limitation in the available classes, which prompted me to pivot
            and push myself further by enrolling in Hack Reactor to obtain my
            Certification as a Full Stack Software Engineer.
          </p>
          <p className="py-2 text-gray-600">
            During this journey, my passion for learning grew even stronger as I
            had the opportunity to explore various aspects of being a Software
            Engineer. I discovered the joy of problem-solving, the importance of
            becoming an autonomous learner, and the significance of fostering
            strong communication within a team. I appreciate the ability to
            apply the concepts I&apos;ve already learned to understand new frameworks
            and programming languages. Moreover, I find great satisfaction in
            using my creativity to bring concepts to life through coding.
          </p>
          <p className="py-2 text-gray-600">
            Becoming a Full Stack Software Engineer offers a diverse range of
            experiences and constantly presents new challenges. I embrace the
            opportunity to solve problems and find innovative solutions. The
            fast-paced nature of the tech industry requires me to be an
            autonomous learner, constantly adapting to new technologies and
            frameworks. Effective communication is also crucial in collaborating
            with team members and ensuring successful project outcomes.
          </p>
          <p className="py-2 text-gray-600">
            I am truly passionate about this field, and I am committed to
            continued growth and development as a Full Stack Software Engineer.
            I will remain curious, eager to learn, and adaptable to the
            ever-evolving landscape of technology. I am excited to see where
            this journey takes me and look forward to making a meaningful impact
            in the industry.
          </p>
          <Link href="/#projects">
            <p className="py-2 text-gray-600 underline cursor-pointer">
              Check out some of my latest projects.
            </p>
          </Link>
        </div>
        <div className="w-full h-auto m-auto shadow-xl shadow-gray-400 rounded-xl flex items-center justify-center p-4 hover:scale-105 ease-in duration-300">
          <Image src={AboutImg} className="rounded-xl" alt="/" />
        </div>
      </div>
    </div>
  );
};

export default About;
