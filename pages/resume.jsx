import React from "react";
import Head from "next/head";
import { FaGitlab, FaLinkedinIn } from "react-icons/fa";

const resume = () => {
  return (
    <>
      <Head>
        <title>Nicholas | Resume</title>
        <meta name="description" content="I'm a full-stack software engineer" />
        <link rel="icon" href="/fav.png" />
      </Head>

      <div className="max-w-[940px] mx-auto p-2 pt-[120px]">
        <h2 className="text-center">Resume</h2>
        <div className="bg-[#d0d4d6] my-4 p-4 w-full flex justify-between items-center">
          <h2 className="text-center">Nicholas Trevino</h2>
          <div className="flex">
            <a
              href="https://www.linkedin.com/in/nicholas-s-trevino/"
              target="_blank"
              rel="noreferrer"

            >
              <FaLinkedinIn size={20} style={{ marginRight: "1rem" }} />
            </a>
            <a
              href="https://gitlab.com/Ntrevino12"
              target="_blank"
              rel="noreferrer"
            >
              <FaGitlab size={20} style={{ marginRight: "1rem" }} />
            </a>
          </div>
        </div>

        {/* Skills */}
        <div className="text-center py-4">
          <h5 className="text-center underline text-[18px] py-2">
            Technical Skills
          </h5>
          <p className="py-2">
            <span className="font-bold">Front End</span>
            <span className="px-2">|</span>DOM manipulation
            <span className="px-2">|</span>Websockets
            <span className="px-2">|</span>React
            <span className="px-2">|</span>React Hooks
            <span className="px-2">|</span>Redux Toolkit
            <span className="px-2">|</span>Boostrap
            <span className="px-2">|</span>TailwindCSS
          </p>
          <p className="py-2">
            <span className="font-bold">Back End</span>
            <span className="px-2">|</span>Django
            <span className="px-2">|</span>RabbitMQ
            <span className="px-2">|</span>PostgreSQL
            <span className="px-2">|</span>MongoDB
            <span className="px-2">|</span>FastAPI
          </p>
          <p className="py-2">
            <span className="font-bold">Programming Languages</span>
            <span className="px-2">|</span>Python
            <span className="px-2">|</span>Javascript ES6+
            <span className="px-2">|</span>SQL
            <span className="px-2">|</span>HTML5
            <span className="px-2">|</span>CSS
          </p>
          <p className="py-2">
            <span className="font-bold">Skills/Tools</span>
            <span className="px-2">|</span>Git
            <span className="px-2">|</span>Microservices
            <span className="px-2">|</span>Domain-Driven Design
            <span className="px-2">|</span>Message Passing
            <span className="px-2">|</span>Event Sourcing
            <span className="px-2">|</span>Docker
          </p>
        </div>

        <h5 className="text-center underline text-[18px] py-4">
          Application Development Experience
        </h5>
        {/* Experience */}
        <div className="py-3">
          <p className="italic">
            <span className="font-bold italic">CruiseControl</span>
            <span className="px-2">|</span>2023
          </p>
          <p className="py-1 italic">
            Python, FastAPI, PostgreSQL, Javascript, React, Boostrap
          </p>
          <ul className="list-disc list-outside px-7 py-1 leading-relaxed">
            <li>
              Full-stack development of appointment and service management
              applications.
            </li>
            <li>
              Utilized agile methodologies, including regular sprints and daily
              stand-up meetings, to ensure efficient development and effective
              collaboration within the team.
            </li>
            <li>
              Developed the backend infrastructure using FastAPI, PostgreSQL,
              and Docker to ensure a robust, scalable, and secure application.
            </li>
            <li>
              Designed and developed the frontend using React and JavaScript,
              ensuring an intuitive and engaging user interface. Leveraged
              continuous integration and deployment practices to streamline the
              development workflow, allowing for regular updates and
              improvements. This approach enabled efficient collaboration and
              facilitated the seamless integration of new features, ensuring a
              smooth and reliable deployment process.
            </li>
            <li>
              Implemented unit testing to ensure consistent code quality
              throughout the project, identifying and addressing issues early
              on, and promoting robust and reliable code.
            </li>
            <li>
              Established GitLab-based continuous integration and deployment
              (CI/CD) pipelines to automate the processes of building, testing,
              and deploying
            </li>
            <li>
              Elevated the user experience by developing a user-friendly website
              that intelligently auto-toggles between three distinct
              authorization levels: customer, business owner, and technicians.
              Leveraging intuitive design principles, we crafted an interface
              that seamlessly adapts to the specific needs and access privileges
              of each user category. By implementing this dynamic authorization
              system, we ensured that users have a smooth and efficient
              experience tailored to their role, enhancing usability and
              satisfaction
            </li>
          </ul>
        </div>
        <div className="py-3">
          <p className="italic">
            <span className="font-bold italic">Car Car</span>
            <span className="px-2">|</span>2023
          </p>
          <p className="py-1 italic">
            Python, Django, Javascript, React, Bootstrap
          </p>
          <ul className="list-disc list-outside px-7 py-1 leading-relaxed">
            <li>
              Contributed to a partnered full-stack development project aimed at
              creating an application for effectively tracking vehicle sales,
              servicing, and inventory. Applied expertise in both front-end and
              back-end development to deliver a streamlined and user-friendly
              solution. Played a key role in integrating essential features,
              resulting in an efficient application that enhances automotive
              operations and drives business success.
            </li>
            <li>
              Collaborated with the team to define API endpoints, handle
              request-response cycles, and ensure adherence to REST principles.
              The result was a robust and scalable microservice that efficiently
              handles sales and services data, facilitating smooth integration
              with other components of the application.
            </li>
            <li>
              Implemented a robust poller mechanism as part of a collaborative
              effort to facilitate seamless communication between microservices.
              Leveraged expertise in system architecture and integration to
              design and develop a highly efficient solution. The poller
              implementation effectively synchronized data and enabled real-time
              communication, enhancing the overall performance and reliability
              of the microservices architecture.
            </li>
            <li>
              Implemented a loosely coupled architecture for the microservices,
              prioritizing ease of maintenance. By carefully designing the
              system with minimal dependencies between microservices, we ensured
              that updates and modifications can be made efficiently and with
              minimal impact on other components. This approach not only
              simplifies maintenance tasks but also enhances scalability and
              flexibility, allowing for seamless integration of new features and
              services as the application evolves.
            </li>
          </ul>
        </div>
        <div className="py-3">
          <p className="italic">
            <span className="font-bold italic">Conference GO</span>
            <span className="px-2">|</span>2023
          </p>
          <p className="py-1 italic">
            Python, Django, Javascript, React, Boostrap
          </p>
          <ul className="list-disc list-outside px-7 py-1 leading-relaxed">
            <li>
              Developed conference management application following
              Domain-Driven Design to allow for expansion of the project if
              needed.
            </li>
            <li>
              Implemented CRUD functionality throughout the application to
              ensure smooth management of user accounts, events, and
              presentations.
            </li>
            <li>
              Utilized the use of an anti-corruption layer in the backend
              portion of events to fetch information from Pexels for the photos
              and Open weather for real-time weather. Both were based on the
              location of the conference.
            </li>
            <li>
              Integrated RabbitMQ for account information retrieval through a
              fanout exchange and poller in which polls information every 5
              seconds in order to synchronize data between the two services.
            </li>
            <li>
              Implemented RabbitMQ work queue integration to automate the
              process of sending email notifications to presenters regarding the
              acceptance or rejection of their conference presentations. This
              integration ensures efficient communication and enhances the
              overall workflow of the application.
            </li>
            <li>
              Implemented Bootstrap to create a responsive frontend that creates
              a smooth fluid experience for the user.
            </li>
          </ul>
        </div>
        <div className="py-3">
          <p className="italic">
            <span className="font-bold italic">Project Alpha</span>
            <span className="px-2">|</span>2022
          </p>
          <p className="py-1 italic">Python, Django, CSS, HTML5</p>
          <ul className="list-disc list-outside px-7 py-1 leading-relaxed">
            <li>
              Implemented authentication and authorization using Django&apos;s
              built-in user model to protect endpoints exclusively for signed-in
              users, ensuring secure access to sensitive features
            </li>
            <li>
              Implemented Django models and views for seamless task and project
              management, enabling essential CRUD operations such as creating,
              reading, updating, and deleting tasks and projects.
            </li>
          </ul>
        </div>

        {/*  */}
        <h5 className="text-center underline text-[18px] py-4">
          Professional Experience
        </h5>

        {/* Experience */}
        <div className="py-1">
          <p className="italic">
            <span className="font-bold">BestBuy</span>
            <span className="px-2">|</span>Antioch, CA
          </p>
          <p className="py-1 italic">Advanced Repair Agent (2018 - Present)</p>
          <ul className="list-disc list-outside px-7 py-1 leading-relaxed">
            <li>
              Communicated with customers in a clear and concise manner,
              explaining technical issues in non-technical terms.
            </li>
            <li>
              Conducted diagnostic tests in a quick and proficient manner to
              identify and fix complex hardware issues and software bugs, with
              the goal of getting the client&apos;s product back to them as promptly
              as possible.
            </li>
            <li>
              Maintained accurate records of repairs of up to 10 repairs on a
              daily basis and inventory of needed resources to complete repairs
              on a weekly basis.
            </li>
          </ul>
        </div>
        <h5 className="text-center underline text-[18px] py-4">Education</h5>

        {/* Education */}
        <div className="py-3">
          <p className="italic">
            <span className="font-bold">Hack Reactor</span>
            <span className="px-2">|</span>Advanced Full-stack Software
            Engineering Certificatation
            <span className="px-2">|</span>2023
          </p>
          <ul className="list-disc list-outside px-7 py-1 leading-relaxed">
            <li>
              Accumulated over 1000 hours of coding experience within an Agile
              environment, resulting in a high level of proficiency in both
              front-end and back-end development. Through iterative and
              collaborative practices, I have honed my skills in delivering
              robust and efficient solutions across the entire software stack.
              This extensive coding experience has equipped me with the ability
              to seamlessly work on both the front-end and back-end aspects of a
              project, ensuring comprehensive expertise and holistic
              contributions.
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default resume;
