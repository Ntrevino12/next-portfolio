import Head from 'next/head'
import { Inter } from 'next/font/google'
import Main from '../components/Main'
import About from '../components/About'
import Contact from '../components/Contact'
import Projects from '../components/Projects'
import Skills from '../components/Skills'


const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <div>
      <Head>
        <title>Nicholas | Front-End Developer</title>
        <meta name="description" content="I’m a Fullstack Developer always pushing the limits and driven to learn more." />
        <link rel="icon" href="/fav.png" />
      </Head>
    <Main/>
    <About />
    <Skills />
    <Projects />
    <Contact />
    </div>
  )
}
