import Image from "next/image";
import React from "react";
import CruiseControlIm from "../public/assets/projects/CruiseControl.png";
import { RiRadioButtonFill } from "react-icons/ri";
import Link from "next/link";

const CruiseControl = () => {
  return (
    <div className="w-full">
      <div className="w-screen h-[50vh] relative">
        <div className="absolute top-0 left-0 w-full h-[50vh] bg-black/70 z-10" />
        <Image
          className="absolute z-1"
          layout="fill"
          objectFit="cover"
          src={CruiseControlIm}
          alt="/"
        />
        <div className="absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] text-white z-10 p-2">
          <h2 className="py-2">CruiseControl App</h2>
          <h3>React JS / Boostrap / FastApi</h3>
        </div>
      </div>

      <div className="max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 py-8">
        <div className="col-span-4">
          <p>Project</p>
          <h2>Overview</h2>
          <p>
            CruiseControl is a full-stack appointment and service
            management application developed using Python, FastAPI, PostgreSQL,
            JavaScript, React, and Bootstrap. The project followed agile
            methodologies, ensuring efficient development and collaboration. The
            backend infrastructure utilized FastAPI, PostgreSQL, and Docker for
            scalability and security. The frontend was designed with React and
            JavaScript, resulting in an intuitive user interface. Continuous
            integration and deployment practices streamlined updates and
            improvements. Unit testing and GitLab-based CI/CD pipelines ensured
            code quality and automation. The standout feature was the dynamic
            authorization system, providing tailored experiences for customers,
            business owners, and technicians. Overall, CruiseControl
            showcases efficient full-stack development and a robust management
            application.
          </p>
          <a
            href="https://gitlab.com/Ntrevino12/cruise-control"
            target="_blank"
            rel="noreferrer"
          >
            <button className="px-8 py-2 mt-4 mr-8">Code</button>
          </a>
          {/* <a
            href=''
            target='_blank'
            rel='noreferrer'
          >
            <button className='px-8 py-2 mt-4'>Demo</button>
          </a> */}
        </div>
        <div className="col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl py-4">
          <div className="p-2">
            <p className="text-center font-bold pb-2">Technologies</p>
            <div className="grid grid-cols-3 md:grid-cols-1">
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> React
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Boostrap
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Javascript
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Python
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" />
                PostgreSQL
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> FastApi
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Docker
              </p>
            </div>
          </div>
        </div>
        <Link href="/#projects">
          <p className="underline cursor-pointer">Back</p>
        </Link>
      </div>
    </div>
  );
};

export default CruiseControl;
